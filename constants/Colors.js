export default {
  primary: "#216583",
  accent: "#f76262",
  gray: "#cccccc",
  grayDarker: "#777777"
};
