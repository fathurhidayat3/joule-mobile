import React from "react";
import { StyleSheet, View, Text } from "react-native";
import NavbarButton from "./NavbarButton";

export default () => (
  <View style={styles.container}>
    <View style={styles.navbarLeft}>
      <NavbarButton name="md-menu" size={22} color="#777777" />
    </View>
    <View style={{ flexGrow: 1, alignItems: "center" }}>
      <Text style={styles.navbarTitle}>joule</Text>
    </View>
    <View style={styles.navbarRight}>
      <NavbarButton name="md-notifications-outline" size={22} color="#777777" />
      <NavbarButton name="md-search" size={22} color="#777777" />
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    backgroundColor: "white",
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc"
  },
  navbarLeft: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    width: 80
  },
  navbarTitle: { fontWeight: "bold", fontSize: 24, color: "#216583" },
  navbarRight: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    width: 80
  }
});
