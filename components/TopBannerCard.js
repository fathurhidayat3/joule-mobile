import React from "react";
import { StyleSheet, TouchableOpacity, ImageBackground } from "react-native";

export default ({ imageUrl }) => {
  return (
    <TouchableOpacity style={styles.container}>
      <ImageBackground
        source={{
          uri: imageUrl
        }}
        imageStyle={{ borderRadius: 8 }}
        style={{ height: "100%", width: "100%" }}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: { marginHorizontal: 8, height: 150, width: 329 }
});
