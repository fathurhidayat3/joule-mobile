import React from "react";
import { StyleSheet, TouchableOpacity, View, Text, Image } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { withNavigation } from "react-navigation";

class PostCard extends React.Component {
  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.props.navigation.navigate("PostDetail")}
      >
        <View style={{ flex: 1, justifyContent: "space-between" }}>
          <Text style={{ fontWeight: "bold", fontSize: 15 }}>
            {this.props.data.header}
          </Text>
          <Text style={{ fontSize: 14, color: "#f76262" }}>KumparanNews</Text>
        </View>
        <View
          style={{
            justifyContent: "space-between",
            alignItems: "flex-end",
            width: 75
          }}
        >
          <Image
            source={{
              uri:
                "https://i0.wp.com/warstek.com/wp-content/uploads/2019/01/sportku-com.jpg?w=1046&ssl=1"
            }}
            style={{ height: 60, width: 60, borderRadius: 4 }}
          />

          <View
            style={{
              flexGrow: 1,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "flex-end",

              width: 60
            }}
          >
            <TouchableOpacity
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Ionicons name={"md-share"} size={22} color={"#777777"} />
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Ionicons name={"md-star-outline"} size={22} color={"#777777"} />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",

    marginTop: 15,
    padding: 16,

    height: 130,

    borderWidth: 1,
    borderColor: "#cccccc",
    borderRadius: 8
  }
});

export default withNavigation(PostCard);
