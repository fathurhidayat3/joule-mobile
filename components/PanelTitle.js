import React from "react";
import { StyleSheet, View, Text } from "react-native";

export default ({titleText}) => (
  <View style={styles.container}>
    <View style={styles.leftPart}>
      <View style={styles.titleLine} />
      <Text style={styles.titleText}>{titleText}</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: { flexDirection: "row", marginTop: 25 },
  leftPart: {
    flexDirection: "row",
    alignItems: "center",
    height: 25
  },
  titleLine: {
    height: 3,
    width: 15,
    backgroundColor: "#f76262",
    borderRadius: 10
  },
  titleText: { marginLeft: 8, fontSize: 20, fontWeight: "bold" }
});
