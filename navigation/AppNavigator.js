import React from "react";
import { Platform } from "react-native";
import {
  createStackNavigator
} from "react-navigation";
import { createAppContainer } from "react-navigation";

import HomeScreen from "../screens/HomeScreen";
import PostDetailScreen from "../screens/PostDetailScreen";

const config = Platform.select({
  web: { headerMode: "screen" },
  default: {}
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    PostDetail: PostDetailScreen
  },
  config
);

export default createAppContainer(HomeStack);
