import * as WebBrowser from "expo-web-browser";
import React from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
  Image,
  ScrollView,
  FlatList
} from "react-native";

import Navbar from "../components/Navbar";

export default function HomeScreen() {
  return (
    <ScrollView style={styles.container}>
      <Image
        source={require("../assets/images/ImageGempa.png")}
        style={{ height: 120, width: "100%" }}
      />

      <View style={styles.contentContainer}>
        <FlatList
          data={[
            "Tak lama ini, dikutip dari media online cnnindonesia.com, beberapa kota di Eropa Barat, Eropa Tengah seperti Paris, Praha, Madrid, Munich, dan Zurich sedang menghadapi gelombang panas. Pada saat terjadi gelombang panas, suhu udara dapat mencapai hingga 41 derajat Celcius. Tak hanya di kota bagian Eropa saja, di India pun sedang menghadapi hal tersebut (gambar 1).",
            "Menurut unit sub bidang Peringatan Dini Iklim BMKG, pengertian gelombang panas adalah periode lanjutan dari cuaca yang panas dan diikuti oleh kelembaban tinggi yang biasa terjadi di wilayah yang sedang mengalami musim panas. Akan tetapi, pengertian tersebut tidak selalu tepat karena tergantung rata – rata suhu harian di suatu wilayah masing – masing. Misalnya saja, ketika rata – rata suhu yang dirasakan oleh penduduk daerah beriklim tropis dianggap sebuah gelombang panas oleh penduduk daerah beriklim dingin.",
            "“Secara umum rata-rata suhu maksimum di wilayah Indonesia berada dalam kisaran 32-36 derajat Celcius”, ujar Kepala Badan Meteorologi, Klimatologi, dan Geofisika, Dwikorita Karnawati, yang dikutip pada media berita online tirto.id bulan Maret 2019 lalu.",
            "Menurut kepala sub bidang prediksi cuaca BMKG, Agie Wandala Putra mengatakan bahwa sistem gelombang panas yang terjadi di sekitar wilayah India tidak bisa terjadi di kawasan Indonesia dikarenakan setiap Negara memiliki pola atau sistem cuaca yang berbeda. Ditambah pula, fenomena gelombang panas yang terjadi disebabkan posisi matahari yang tengah berada di bagian utara bumi sehingga pemanasan terjadi cukup intens (gambar 2)."
          ]}
          renderItem={({ item }) => {
            return (
              <Text style={{ marginVertical: 8, fontSize: 14 }}>{item}</Text>
            );
          }}
          keyExtractor={(_, index) => `${index}`}
        />
      </View>

      <View
        style={{
          alignItems: "center",
          padding: 16,
          height: 515,
          backgroundColor: "#EEEEEE"
        }}
      >
        <Image
          source={require("../assets/images/icon.png")}
          style={{ height: 50, width: 50 }}
        />
      </View>
    </ScrollView>
  );
}

HomeScreen.navigationOptions = {
  header: () => {
    return <Navbar />;
  }
};

const styles = StyleSheet.create({
  contentContainer: {
    padding: 16
  }
});
