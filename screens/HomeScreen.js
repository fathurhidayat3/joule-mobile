import * as WebBrowser from "expo-web-browser";
import React from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
  Image,
  ScrollView,
  FlatList
} from "react-native";

import Navbar from "../components/Navbar";
import PanelTitle from "../components/PanelTitle";
import PostCard from "../components/PostCard";
import TopBannerCard from "../components/TopBannerCard";

export default function HomeScreen() {
  return (
    <ScrollView style={styles.container}>
      <View style={styles.contentContainer}>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{ marginHorizontal: -8 }}
          data={[
            "https://i0.wp.com/warstek.com/wp-content/uploads/2019/01/sportku-com.jpg?w=1046&ssl=1",
            "https://i0.wp.com/warstek.com/wp-content/uploads/2019/01/sportku-com.jpg?w=1046&ssl=1"
          ]}
          renderItem={({ item }) => {
            return <TopBannerCard imageUrl={item} />;
          }}
          keyExtractor={(_, index) => `${index}`}
        />

        <PanelTitle titleText="Populer" />

        <FlatList
          data={[
            {
              header:
                "Gelombang Panas Melanda Luar Negeri, Akankah Berdampak ke Indonesia?"
            },
            {
              header:
                "Tidak Kalah Dengan Amerika, Perusahaan Kendaraan Listrik Kini Ada Di Indonesia"
            }
          ]}
          renderItem={({ item }) => {
            return <PostCard data={item} />;
          }}
          keyExtractor={(_, index) => `${index}`}
        />

        <PanelTitle titleText="Dunia" />

        <FlatList
          data={[
            {
              header:
                "Gelombang Panas Melanda Luar Negeri, Akankah Berdampak ke Indonesia?"
            },
            {
              header:
                "Tidak Kalah Dengan Amerika, Perusahaan Kendaraan Listrik Kini Ada Di Indonesia"
            }
          ]}
          renderItem={({ item }) => {
            return <PostCard data={item} />;
          }}
          keyExtractor={(_, index) => `${index}`}
        />

        <PanelTitle titleText="Terkini" />

        <FlatList
          data={[
            {
              header:
                "Tidak Kalah Dengan Amerika, Perusahaan Kendaraan Listrik Kini Ada Di Indonesia"
            },
            {
              header:
                "Siringmakar 13: Mendalami Proses Produksi Pulp dan Kertas"
            }
          ]}
          renderItem={({ item }) => {
            return <PostCard data={item} />;
          }}
          keyExtractor={(_, index) => `${index}`}
        />

        <PanelTitle titleText="Fashion" />

        <FlatList
          data={[
            {
              header:
                "Gelombang Panas Melanda Luar Negeri, Akankah Berdampak ke Indonesia?"
            },
            {
              header:
                "Siringmakar 13: Mendalami Proses Produksi Pulp dan Kertas"
            }
          ]}
          renderItem={({ item }) => {
            return <PostCard data={item} />;
          }}
          keyExtractor={(_, index) => `${index}`}
        />
      </View>

      <View
        style={{
          alignItems: "center",
          padding: 16,
          height: 515,
          backgroundColor: "#EEEEEE"
        }}
      >
        <Image
          source={require("../assets/images/icon.png")}
          style={{ height: 50, width: 50 }}
        />
      </View>
    </ScrollView>
  );
}

HomeScreen.navigationOptions = {
  header: () => {
    return <Navbar />;
  }
};

const styles = StyleSheet.create({
  contentContainer: {
    padding: 16
  }
});
